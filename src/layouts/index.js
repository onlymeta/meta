import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Helmet from 'react-helmet'

import Menu from '../components/Menu/Menu'
import Logo from '../components/Logo/Logo'

import './index.css'
import '../assets/fonts/fonts.css'

const StyledIndexContainer = styled.div`
  @media screen and (max-width: 700px) {
    height: 100%;
  }
`

const StyledContainer = styled.div`
  display: flex;
  margin: auto;
  padding: 58px 0;
  height: auto;
  @media screen and (min-width: 320px) {
    max-width: 280px;
  }
  @media screen and (min-width: 380px) {
    max-width: 340px;
  }
  @media screen and (min-width: 576px) {
    max-width: 510px;
  }
  @media screen and (min-width: 768px) {
    max-width: 690px;
  }
  @media screen and (min-width: 992px) {
    max-width: 930px;
  }
  @media screen and (min-width: 1200px) {
    max-width: 1110px;
  }
  @media screen and (max-width: 767px) {
    flex-flow: column;
    align-items: center;
  }
`

const Sidebar = styled.div`
  width: 223px;
  @media screen and (max-width: 767px) {
    flex-flow: column;
    align-items: center;
    margin: 0px;
  }
  @media screen and (min-width: 768px) {
    margin-right: 62px;
  }
`

const Content = styled.div`
  padding-top: 80px;
  flex: 1;
`
class Layout extends Component {
  _setInitialMenuState = () => {
    const clientWidth =
      typeof window !== 'undefined' && document.body.clientWidth
    return clientWidth >= 768
  }

  render() {
    const {
      location: { pathname },
      children,
      data: {
        site: {
          siteMetadata: { title, metaDescription },
        },
      },
    } = this.props

    const head = (
      <Helmet
        titleTemplate={`%s | ${title}`}
        defaultTitle={title}
        meta={[
          { 'http-equiv': 'Content-Type', content: 'text/html; charset=utf-8' },
          { name: 'description', content: metaDescription },
        ]}
      />
    )

    const conditionalLayout =
      pathname === '/' ? (
        <StyledIndexContainer>
          {head}
          {children()}
        </StyledIndexContainer>
      ) : (
        <StyledContainer>
          {head}
          <Sidebar>
            <Logo />
            <Menu expanded={this._setInitialMenuState()} absolute />
          </Sidebar>
          <Content>{children()}</Content>
        </StyledContainer>
      )

    return conditionalLayout
  }
}

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
        metaDescription
      }
    }
  }
`
