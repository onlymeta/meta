import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import Cube from '../Cube/Cube'

const StyledTitle = styled.div`
  display: flex;
  flex-shrink: 0;
  justify-content: space-between;
  align-items: center;
  margin: ${({ cubeSize }) => cubeSize * 0.08 + 'px'};
  flex-flow: row;
  width: ${({ cubeSize }) => (cubeSize + cubeSize * 0.25) * 4 + 'px'};
  user-select: none;
  transition: transform 0.2s ease-in-out;
  @media screen and (max-width: 700px) {
    transform: scale(0.9);
  }
  @media screen and (max-width: 630px) {
    transform: scale(0.8);
  }
  @media screen and (max-width: 560px) {
    transform: scale(0.7);
  }
  @media screen and (max-width: 490px) {
    transform: scale(0.6);
  }
  @media screen and (max-width: 420px) {
    transform: scale(0.5);
  }
  @media screen and (max-width: 350px) {
    transform: scale(0.4);
  }
`

const Title = ({ cubeSize }) => {
  return (
    <StyledTitle cubeSize={cubeSize}>
      <Cube sideSize={cubeSize} letter={'M'} />
      <Cube sideSize={cubeSize} letter={'E'} />
      <Cube sideSize={cubeSize} letter={'T'} />
      <Cube sideSize={cubeSize} letter={'A'} />
    </StyledTitle>
  )
}

Title.defaultProps = {
  cubeSize: 122,
}

Title.propTypes = {
  cubeSize: PropTypes.number,
}

export default Title
