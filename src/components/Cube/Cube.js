import React, { PureComponent } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import letters from './cubeImgs'

const StyledContainer = styled.div`
  width: ${({ size }) => size + 'px'};
  height: ${({ size }) => size + 'px'};
  perspective: ${({ size }) => size * 4 + 'px'};
`

const StyledCube = styled.div`
  height: ${({ size }) => size + 'px'};
  width: ${({ size }) => size + 'px'};
  transform-style: preserve-3d;
  transition: transform 0.5s ease-in-out;
  transform: ${({ degrees }) => `rotateY(${degrees}deg)`};
`

const CubeSide = styled.div`
  position: absolute;
  left: 0px;
  top: 0px;
  width: ${({ size }) => size + 'px'};
  height: ${({ size }) => size + 'px'};
  background-color: white;
`

const SideA = styled(CubeSide)`
  transform: rotateY(0deg) ${({ size }) => `translateZ(${size / 2}px)`};
`

const SideB = styled(CubeSide)`
  transform: rotateY(90deg) ${({ size }) => `translateZ(${size / 2}px)`};
`

const SideC = styled(CubeSide)`
  transform: rotateY(180deg) ${({ size }) => `translateZ(${size / 2}px)`};
`

const SideD = styled(CubeSide)`
  transform: rotateY(270deg) ${({ size }) => `translateZ(${size / 2}px)`};
`

const SideImg = styled.img`
  width: 100%;
  height: 100%;
`
class Cube extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      // Initiate element without rotation ( CSS transform: rotateY(0deg) )
      rotationOnY: 0,
      // Actual cube side size after parent element CSS perspective is set
      perspectiveSideSize: props.sideSize + 18,
    }
  }

  onMouseEnterRotation = e => {
    const { perspectiveSideSize } = this.state

    // Calculate mouseenter event direction
    const offsetLeft =
      e.target.getBoundingClientRect().left + document.body.scrollLeft
    const x = (e.pageX - offsetLeft - perspectiveSideSize / 2) * 1
    const direction = x > 0 ? 'right' : 'left'

    this.setState(prevState => {
      return {
        // Add or substract degrees to transform CSS property
        rotationOnY: prevState.rotationOnY + (direction === 'left' ? 90 : -90),
      }
    })
  }

  render() {
    const { sideSize, letter } = this.props
    const { rotationOnY } = this.state
    const images = letters[letter.toLowerCase()]
    return (
      <StyledContainer size={sideSize}>
        <StyledCube
          innerRef={el => (this.cubeRef = el)}
          size={sideSize}
          degrees={rotationOnY}
          onMouseEnter={e => this.onMouseEnterRotation(e)}
        >
          <SideA size={sideSize}>
            <SideImg src={images[0]} />
          </SideA>
          <SideB size={sideSize}>
            <SideImg src={images[1]} />
          </SideB>
          <SideC size={sideSize}>
            <SideImg src={images[2]} />
          </SideC>
          <SideD size={sideSize}>
            <SideImg src={images[3]} />
          </SideD>
        </StyledCube>
      </StyledContainer>
    )
  }
}

Cube.defaultProps = {
  sideSize: 122,
  letter: 'M',
}

Cube.propTypes = {
  sideSize: PropTypes.number,
  letter: PropTypes.oneOf(['M', 'E', 'T', 'A', 'm', 'e', 't', 'a']),
}

export default Cube
