import M1 from '../../assets/images/title/m.svg'
import M2 from '../../assets/images/title/m-2.jpg'
import M3 from '../../assets/images/title/m-3.jpg'
import M4 from '../../assets/images/title/m-4.jpg'
import E1 from '../../assets/images/title/e.svg'
import E2 from '../../assets/images/title/e-2.jpg'
import E3 from '../../assets/images/title/e-3.jpg'
import E4 from '../../assets/images/title/e-4.jpg'
import T1 from '../../assets/images/title/t.svg'
import T2 from '../../assets/images/title/t-2.jpg'
import T3 from '../../assets/images/title/t-3.jpg'
import T4 from '../../assets/images/title/t-4.jpg'
import A1 from '../../assets/images/title/a.svg'
import A2 from '../../assets/images/title/a-2.jpg'
import A3 from '../../assets/images/title/a-3.jpg'
import A4 from '../../assets/images/title/a-4.jpg'

const letters = {
  m: [M1, M2, M3, M4],
  e: [E1, E2, E3, E4],
  t: [T1, T2, T3, T4],
  a: [A1, A2, A3, A4],
}

export default letters
