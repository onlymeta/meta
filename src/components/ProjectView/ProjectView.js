import React from 'react'
import styled from 'styled-components'
import { StyledTitle } from '../shared/styled'

const StyledImage = styled.img`
  width: 100%;
  object-fit: cover;
  @media screen and (min-width: 320px) {
    height: 207px;
    margin-bottom: 40px;
  }
  @media screen and (min-width: 769px) {
    height: 414px;
    margin-bottom: 80px;
  }
`

const ProjectView = ({ name, imageArray }) => (
  <div>
    <StyledTitle>{name}</StyledTitle>
    {imageArray.map((src, i) => (
      <StyledImage key={i} src={src} />
    ))}
  </div>
)

export default ProjectView
