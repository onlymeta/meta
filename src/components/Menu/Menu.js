import React, { Component } from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import Transition from 'react-transition-group/Transition'

import arrow from '../../assets/images/arrow-down.svg'

const initialMenuHeight = 25
const optionHeight = 50
const transitionDuration = 100

const MenuBox = styled.div`
  position: ${({ absolute }) => (absolute ? 'absolute' : 'relative')};
  z-index: 20;
`
const MenuShadow = styled.div`
  min-width: 217px;
  min-height: ${initialMenuHeight}px;
  height: ${({ height }) => height + 'px'};
  transition: height 0.2s ease-in-out;
  background-color: white;
  border-bottom: 1px solid #1d1d1d;
  border-right: 1px solid #1d1d1d;
  position: absolute;
  left: 5px;
  top: 5px;
`

const MenuContainer = styled.div`
  border: 1px solid #1d1d1d;
  border-bottom: ${({ height }) => height === initialMenuHeight && 'none'};
  background-color: white;
  width: 217px;
  position: relative;
  z-index: 1;
  height: ${({ height }) => height + 'px'};
  transition: height 0.2s ease-in-out;
`

const MenuHeader = styled.div`
  width: 100%;
  height: 24px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-family: 'Orator Std', monospace, monospace;
  border-bottom: 1px solid #1d1d1d;
`

const MenuCaretBox = styled.div`
  height: 24px;
  width: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-left: 1px solid #1d1d1d;
  cursor: pointer;
  user-select: none;
`

const MenuArrow = styled.img`
  width: 14px;
  object-fit: contain;
  transform: ${({ expanded }) => (expanded ? 'rotate(180deg)' : 'none')};
  transition: transform 0.2s ease-in-out;
`

const MenuLink = styled(Link).attrs({
  activeClassName: 'active',
})`
  display: block;
  text-decoration: none;
  &:not(:first-of-type) {
    border-top: 1px solid #1d1d1d;
  }
  &.active > div {
    background-color: #1d1d1d;
    color: white;
  }
`

const MenuItem = styled.div`
  line-height: 1;
  display: flex;
  align-items: center;
  background-color: white;
  color: #1d1d1d;
  font-family: 'Orator Std', monospace, monospace;
  padding-left: 17px;
  user-select: none;
`

const MenuOption = styled(MenuItem)`
  width: 100%;
  height: 50px;
  &:hover {
    background-color: #d8d8d8;
  }
`

const AnimatedOpacity = styled.div`
  transition: opacity ${transitionDuration}ms ease-in-out;
  opacity: ${({ animationState }) => (animationState === 'entered' ? 1 : 0)};
`

class Menu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      options: [
        { name: 'about_us', route: '/about_us' },
        { name: 'our_work', route: '/our_work' },
        // { name: 'news', route: '/news' },
        { name: 'contact_us', route: '/contact_us' },
      ],
      expanded: props.expanded,
    }
  }

  toggleExpand = () => {
    this.setState(prevState => {
      return { expanded: !prevState.expanded }
    }, this.conditionalScrollBottom())
  }

  conditionalToggleExpand = () => {
    const clientWidth = document.body.clientWidth
    clientWidth <= 768 && this.toggleExpand()
  }

  conditionalScrollBottom = () => {
    const { currentPage } = this.props
    const clientWidth = document.body.clientWidth
    currentPage === '/' && clientWidth >= 700 && this._scrollToBottom()
  }

  _scrollToBottom = () => {
    setTimeout(() => {
      window.scrollTo({
        top: document.body.scrollHeight,
        behavior: 'smooth',
      })
    }, 200)
  }

  _mapMenuItems = () =>
    this.state.options.map(opt => (
      <MenuLink
        key={opt.name}
        to={opt.route}
        onClick={this.conditionalToggleExpand}
      >
        <MenuOption>
          <span>{opt.name.toUpperCase()}</span>
        </MenuOption>
      </MenuLink>
    ))

  render() {
    const { expanded, options } = this.state
    const { absolute } = this.props
    const items = (
      <Transition
        in={expanded}
        timeout={transitionDuration}
        mountOnEnter
        unmountOnExit
      >
        {state => (
          <AnimatedOpacity animationState={state}>
            {this._mapMenuItems()}
          </AnimatedOpacity>
        )}
      </Transition>
    )
    const menuBoxHeight = expanded
      ? options.length * optionHeight + options.length + initialMenuHeight
      : initialMenuHeight

    return (
      <MenuBox absolute={absolute}>
        <MenuContainer height={menuBoxHeight}>
          <MenuHeader>
            <MenuItem>
              <span>META-MENU</span>
            </MenuItem>
            <MenuCaretBox onClick={this.toggleExpand}>
              <MenuArrow src={arrow} expanded={expanded} />
            </MenuCaretBox>
          </MenuHeader>
          {items}
        </MenuContainer>
        <MenuShadow height={menuBoxHeight} />
      </MenuBox>
    )
  }
}

export default Menu
