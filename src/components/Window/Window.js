import React, { Component } from 'react'
import { OutboundLink } from 'gatsby-plugin-google-analytics'
import styled from 'styled-components'

const minHeight = 24

const WindowBox = styled.div`
  position: relative;
`
const WindowShadow = styled.div`
  width: calc(100% - 5px);
  min-height: ${minHeight}px;
  height: ${({ height }) => (height ? `${height}px` : 'auto')};
  border-bottom: 1px solid #1d1d1d;
  border-right: 1px solid #1d1d1d;
  position: absolute;
  left: 5px;
  top: 5px;
`

const WindowContainer = styled.div`
  width: calc(100% - 5px);
  border: 1px solid #1d1d1d;
  background-color: white;
  position: relative;
  z-index: 1;
`

export const WindowHeader = styled.div`
  width: 100%;
  height: ${minHeight}px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-family: 'Orator Std', monospace, monospace;
  border-bottom: 1px solid #1d1d1d;
`

export const WindowButtonBox = styled.div`
  height: ${minHeight}px;
  width: auto;
  padding: 0px 17px 0px 17px;
  background-color: #1d1d1d;
  color: white;
  display: flex;
  justify-content: center;
  text-transform: uppercase;
  align-items: center;
  border-left: 1px solid #1d1d1d;
  cursor: pointer;
  user-select: none;
`

const WindowItem = styled.div`
  line-height: 1;
  display: flex;
  align-items: center;
  background-color: white;
  color: #1d1d1d;
  font-family: 'Orator Std', monospace, monospace;
  padding-left: 17px;
  user-select: none;
`

const WindowTitle = styled.span`
  text-transform: uppercase;
`

const StyledLink = styled(OutboundLink)`
  text-decoration: none;
`

class Window extends Component {
  state = {
    componentHeight: null,
  }

  componentDidMount() {
    this._setComponentHeight()
    window.addEventListener('resize', this._setComponentHeight)
  }

  componentDidUpdate(prevProps) {
    const { children } = this.props
    const { children: prevChildren } = prevProps

    children !== prevChildren && this._setComponentHeight()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this._setComponentHeight)
  }

  _setComponentHeight = () => {
    const windowHeight = this.container.offsetHeight
    this.setState({ componentHeight: windowHeight })
  }

  render() {
    const { children, title, action } = this.props
    const { componentHeight } = this.state

    return (
      <WindowBox>
        <WindowContainer innerRef={el => (this.container = el)}>
          <WindowHeader>
            <WindowItem>
              <WindowTitle>{title}</WindowTitle>
            </WindowItem>
            {action && (
              <StyledLink href={action.link} target="_blank">
                <WindowButtonBox>
                  <span>{action.title}</span>
                </WindowButtonBox>
              </StyledLink>
            )}
          </WindowHeader>
          {children}
        </WindowContainer>
        <WindowShadow height={componentHeight} />
      </WindowBox>
    )
  }
}

export default Window
