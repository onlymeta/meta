import React from 'react'
import styled from 'styled-components'
import Window, {
  WindowHeader,
  WindowButtonBox,
} from '../../components/Window/Window'

const StyledSubmit = styled.button.attrs({
  type: 'submit',
})`
  outline: none;
  border: none;
  -webkit-appearance: none;
  font-family: 'Orator Std', monospace, monospace;
  text-transform: uppercase;
  font-size: 16px;
`

const StyledInput = styled.input.attrs({
  placeholder: 'Your Email',
  type: 'email',
  required: true,
})`
  flex: 1;
  padding: 0px 6px 0px 17px;
  border: none;
  color: #1d1d1d;
  font-family: 'Orator Std', monospace, monospace;
  text-transform: uppercase;
  background-color: transparent;
  outline: none;
  font-size: 16px;
  &:focus {
    &::placeholder {
      color: #d8d8d8;
    }
  }
`

const StyledTextarea = styled.textarea.attrs({
  placeholder: 'Your Message',
  rows: '6',
  required: true,
})`
  width: 100%;
  height: auto;
  resize: none;
  outline: none;
  border: none;
  color: #1d1d1d;
  font-family: 'Orator Std', monospace, monospace;
  text-transform: uppercase;
  background-color: transparent;
  padding: 0px 6px 0px 17px;
  font-size: 16px;
  &:focus {
    &::placeholder {
      color: #d8d8d8;
    }
  }
`

const ContactForm = () => (
  <Window title="Email Us">
    <form data-netlify="true" name="contact-us" method="POST">
      <WindowHeader>
        <StyledInput name="email" />
        <StyledSubmit>
          <WindowButtonBox>
            <span>Send</span>
          </WindowButtonBox>
        </StyledSubmit>
      </WindowHeader>
      <StyledTextarea name="message" />
      <input type="hidden" name="form-name" value="contact-us" />
      <input
        type="hidden"
        name="subject"
        value="Nuevo mensaje desde onlymeta.com"
      />
    </form>
  </Window>
)

export default ContactForm
