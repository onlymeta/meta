import React from 'react'
import Window from '../Window/Window'
import styled from 'styled-components'

const StyledFrame = styled.iframe`
  border: none;
  overflow: hidden;
  width: 100%;
  padding: 5px 5px 0 5px;
  @media screen and (min-width: 320px) {
    height: ${(90.33 + 5) * 2}px;
  }
  @media screen and (min-width: 380px) {
    height: ${(95 + 5) * 2}px;
  }
  @media screen and (min-width: 768px) {
    height: ${(125.984 + 5) * 2}px;
  }
  @media screen and (min-width: 992px) {
    height: ${(98.66 + 5) * 2}px;
  }
  @media screen and (min-width: 1200px) {
    height: ${(128.66 + 5) * 2}px;
  }
`

const URL = 'https://www.instagram.com/onlymeta/'

const InstagramGrid = () => (
  <Window title="Visit Us" action={{ title: 'Instagram', link: URL }}>
    <script src="https://snapwidget.com/js/snapwidget.js" />
    <StyledFrame
      src="https://snapwidget.com/embed/597947"
      className="snapwidget-widget"
      allowTransparency="true"
      frameBorder="0"
      scrolling="no"
    />
  </Window>
)

export default InstagramGrid
