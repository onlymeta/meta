import React, { PureComponent } from 'react'
import Scrollbars from 'react-custom-scrollbars'

class ScrollableContainer extends PureComponent {
  renderTrack = props => (
    <div
      style={{
        ...props.style,
        borderTop: '14px solid #1d1d1d',
        borderBottom: '14px solid #1d1d1d',
        width: '14px',
        backgroundColor: 'transparent',
        top: 0,
        right: 0,
        bottom: 0,
      }}
    />
  )

  renderThumb = props => (
    <div
      style={{
        ...props.style,
        width: '14px',
        backgroundColor: '#1d1d1d',
      }}
    />
  )

  renderView = props => (
    <div
      style={{
        ...props.style,
        paddingRight: 30,
      }}
    />
  )

  render() {
    const { children } = this.props
    return (
      <Scrollbars
        universal
        autoHeight
        autoHeightMin={400}
        renderThumbVertical={props => this.renderThumb(props)}
        renderTrackVertical={props => this.renderTrack(props)}
        renderView={props => this.renderView(props)}
      >
        {children}
      </Scrollbars>
    )
  }
}

export default ScrollableContainer
