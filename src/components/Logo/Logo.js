import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'
import logo from '../../assets/images/logo.svg'

const LogoLink = styled(Link)`
  margin-bottom: 21px;
  display: block;
`

const ImgLogo = styled.img`
  width: 100%;
`

const Logo = () => (
  <LogoLink to="/">
    <ImgLogo src={logo} />
  </LogoLink>
)

export default Logo