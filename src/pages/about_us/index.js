import React from 'react'
import styled from 'styled-components'
// import ScrollableContainer from '../../components/ScrollableContainer/ScrollableContainer'
import { StyledTitle } from '../../components/shared/styled'
import Helmet from 'react-helmet'

const content = `Meta: (1) pref: (del griego. <i>Metá</i>), más allá, después, por el medio, y también cambio o mutación.

(2) agcia: (del creativo <i>Beyond</i>), grupo de profesionales con experiencia en branding, publicidad, programación y diseño reunidos bajo el nombre de un nuevo estudio creativo, que junta sus talentos e ideas en función de hacer llegar las marcas aliadas hasta el siguiente nivel.

(Definiciones no aprobadas por la RAE… todavía)`

const StyledContainer = styled.div`
  width: 100%;
  height: 100%;
`

const StyledTextContent = styled.p`
  font-family: 'Nunito Sans', sans-serif;
  text-align: justify;
  text-align-last: right;
  font-weight: 300;
  font-size: 18px;
  margin-bottom: 18px;

  /* Safari fix */
  @media not all and (min-resolution: 0.001dpcm) {
    @media {
      text-align: right;
    }
  }
`
const pageTitle = 'ABOUT_US'

const AboutUsPage = () => {
  const paragraphs = content
    .split('\n')
    .map((p, i) => (
      <StyledTextContent key={i} dangerouslySetInnerHTML={{ __html: p }} />
    ))
  return (
    <StyledContainer>
      <Helmet title={pageTitle} />
      <StyledTitle>{pageTitle}</StyledTitle>
      {/* <ScrollableContainer>{paragraphs}</ScrollableContainer> */}
      <div>{paragraphs}</div>
    </StyledContainer>
  )
}

export default AboutUsPage
