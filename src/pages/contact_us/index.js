import React from 'react'
import styled from 'styled-components'
import Helmet from 'react-helmet'
import { StyledTitle } from '../../components/shared/styled'
import InstagramGrid from '../../components/InstagramGrid/InstagramGrid'
import ContactForm from '../../components/ContactForm/ContactForm'

const StyledContainer = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  min-height: 500px;
  column-gap: 10px;
  row-gap: 30px;
  @media screen and (min-width: 320px) {
    grid-template-columns: 100%;
    grid-template-rows: auto auto;
  }
  @media screen and (min-width: 992px) {
    grid-template-columns: 50% 50%;
    grid-template-rows: 100%;
  }
`

const FooterTitle = styled(StyledTitle)`
  margin-top: 10px;
`

const pageTitle = 'CONTACT_US'

const ContactUsPage = () => (
  <StyledContainer>
    <Helmet title={pageTitle} />
    <InstagramGrid />
    <div>
      <ContactForm />
      <FooterTitle>{pageTitle}</FooterTitle>
    </div>
  </StyledContainer>
)

export default ContactUsPage
