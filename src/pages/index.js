import React from 'react'
import styled from 'styled-components'
import Title from '../components/Title/Title'
import Menu from '../components/Menu/Menu'

const StyledContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-flow: column;
  @media screen and (max-width: 700px) {
    height: 100%;
    align-items: center;
    justify-content: center;
  }
  @media screen and (min-width: 701px) {
    position: absolute;
    top: 50%;
    left: 50%;
    margin: -70px 0 0 -315px;
    align-items: flex-end;
  }
`

const MenuBox = styled.div`
  padding: 50px 6px 50px 0;
`

const IndexPage = ({ location: { pathname } }) => {
  return (
    <StyledContainer>
      <Title />
      <MenuBox>
        <Menu currentPage={pathname} />
      </MenuBox>
    </StyledContainer>
  )
}

export default IndexPage
