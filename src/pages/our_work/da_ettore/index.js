import React from 'react'
import ProjectView from '../../../components/ProjectView/ProjectView'
import Helmet from 'react-helmet'

const images = [
  require('../../../assets/images/da_ettore/Menu1.jpg'),
  require('../../../assets/images/da_ettore/Manu2.jpg'),
  require('../../../assets/images/da_ettore/Menu3.jpg'),
  require('../../../assets/images/da_ettore/Menu4.jpg'),
  require('../../../assets/images/da_ettore/RRSS1.jpg'),
  require('../../../assets/images/da_ettore/RRSS2.jpg'),
  require('../../../assets/images/da_ettore/RRSS3.jpg'),
  require('../../../assets/images/da_ettore/RRSS4.jpg'),
  require('../../../assets/images/da_ettore/RRSS5.jpg'),
]

const pageTitle = 'Da Ettore'

const DaEttore = () => (
  <div>
    <Helmet title={pageTitle} />
    <ProjectView name={pageTitle} imageArray={images} />
  </div>
)

export default DaEttore
