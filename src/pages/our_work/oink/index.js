import React from 'react'
import ProjectView from '../../../components/ProjectView/ProjectView'
import Helmet from 'react-helmet'

const images = [
  require('../../../assets/images/oink/RRSS1-2.jpg'),
  require('../../../assets/images/oink/RRSS2-2.jpg'),
  require('../../../assets/images/oink/RRSS3-2.jpg'),
  require('../../../assets/images/oink/RRSS4-2.jpg'),
  require('../../../assets/images/oink/RRSS5-2.jpg'),
]

const pageTitle = 'Oink'

const Oink = () => (
  <div>
    <Helmet title={pageTitle} />
    <ProjectView name={pageTitle} imageArray={images} />
  </div>
)

export default Oink
