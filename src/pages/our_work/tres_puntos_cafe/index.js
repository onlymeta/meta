import React from 'react'
import ProjectView from '../../../components/ProjectView/ProjectView'
import Helmet from 'react-helmet'

const images = [
  require('../../../assets/images/tres_puntos_cafe/Cookies1.jpg'),
  require('../../../assets/images/tres_puntos_cafe/Cookies2.jpg'),
  require('../../../assets/images/tres_puntos_cafe/Cookies3.jpg'),
  require('../../../assets/images/tres_puntos_cafe/Poster1.jpg'),
  require('../../../assets/images/tres_puntos_cafe/Poster2.jpg'),
  require('../../../assets/images/tres_puntos_cafe/Poster3.jpg'),
  require('../../../assets/images/tres_puntos_cafe/RRSS1.jpg'),
  require('../../../assets/images/tres_puntos_cafe/RRSS2.jpg'),
  require('../../../assets/images/tres_puntos_cafe/RRSS3.jpg'),
  require('../../../assets/images/tres_puntos_cafe/RRSS4.jpg'),
  require('../../../assets/images/tres_puntos_cafe/RRSS5.jpg'),
]

const pageTitle = 'Tres Puntos Café'

const TresPuntosCafe = () => (
  <div>
    <Helmet title={pageTitle} />
    <ProjectView name={pageTitle} imageArray={images} />
  </div>
)

export default TresPuntosCafe
