import React, { PureComponent } from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import Helmet from 'react-helmet'

const Image = styled.img`
  width: 100%;
  height: 100%;
`

const ImageOverlay = styled.div`
  padding: 20px;
  text-align: right;
  position: absolute;
  font-family: 'Nunito Sans', sans-serif;
  font-weight: 900;
  text-transform: uppercase;
  width: 100%;
  height: 100%;
  justify-content: flex-end;
  align-items: center;
  top: 0;
  background-color: rgba(0, 0, 0, 0.6);
  z-index: 10;
  color: white;
  display: flex;
  transition: opacity 0.2s ease-in-out;
  opacity: 0;
  &:hover {
    opacity: 1;
  }
`

const images = [
  {
    source: require('../../assets/images/our_work/tile_daettore.jpg'),
    alt: 'Da Ettore',
    link: '/our_work/da_ettore',
  },
  {
    source: require('../../assets/images/our_work/tile_flordelis.jpg'),
    alt: 'Flor De Lis',
    link: '/our_work/flor_de_lis',
  },
  {
    source: require('../../assets/images/our_work/tile_manu.jpg'),
    alt: 'Da Ettore',
    link: '/our_work/da_ettore',
  },
  {
    source: require('../../assets/images/our_work/tile_oink.jpg'),
    alt: 'Oink',
    link: '/our_work/oink',
  },
  {
    source: require('../../assets/images/our_work/tile_trespuntos1.jpg'),
    alt: 'Tres Puntos Café',
    link: '/our_work/tres_puntos_cafe',
  },
  {
    source: require('../../assets/images/our_work/tile_trespuntos2.jpg'),
    alt: 'Tres Puntos Café',
    link: '/our_work/tres_puntos_cafe',
  },
  {
    source: require('../../assets/images/our_work/tile_trespuntos3.jpg'),
    alt: 'Tres Puntos Café',
    link: '/our_work/tres_puntos_cafe',
  },
]

const shuffle = array => {
  let currentIndex = array.length,
    temporaryValue,
    randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

const pageTitle = 'OUR_WORK'

class OurWorkPage extends PureComponent {
  state = {
    cellHeight: 207,
    cols: 4,
  }

  _calculateCellHeight = () => {
    const windowWidth = window.innerWidth
    let cellHeight = 207
    if (windowWidth >= 320) {
      cellHeight = 140
    }
    if (windowWidth >= 380) {
      cellHeight = 170
    }
    if (windowWidth >= 576) {
      cellHeight = 255
    }
    if (windowWidth >= 768) {
      cellHeight = 202.5
    }
    if (windowWidth >= 992) {
      cellHeight = 161.25
    }
    if (windowWidth >= 1200) {
      cellHeight = 206.25
    }
    this.setState({ cellHeight })
  }

  _calculateTileSize = () => {
    const windowWidth = window.innerWidth
    this.setState({
      cols: windowWidth >= 992 ? 4 : 2,
    })
  }

  calculateAll = () => {
    this._calculateCellHeight()
    this._calculateTileSize()
  }

  componentDidMount() {
    this._calculateCellHeight()
    this._calculateTileSize()
    window.addEventListener('resize', this.calculateAll)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.calculateAll)
  }

  render() {
    const { cellHeight, cols } = this.state
    const imageArray = shuffle(images)
    const tiles = imageArray.map(({ source, alt, link }, i) => (
      <GridListTile key={source} cols={i === 6 ? 2 : 1} rows={i === 6 ? 2 : 1}>
        <Link to={link}>
          <Image src={source} alt={alt} />
          <ImageOverlay>{alt}</ImageOverlay>
        </Link>
      </GridListTile>
    ))

    return (
      <div>
        <Helmet title={pageTitle} />
        <GridList cols={cols} spacing={0} cellHeight={cellHeight}>
          {tiles}
        </GridList>
      </div>
    )
  }
}

export default OurWorkPage
