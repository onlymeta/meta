import React from 'react'
import ProjectView from '../../../components/ProjectView/ProjectView'
import Helmet from 'react-helmet'

const images = [
  require('../../../assets/images/flor_de_lis/Brand1.jpg'),
  require('../../../assets/images/flor_de_lis/Brand2.jpg'),
  require('../../../assets/images/flor_de_lis/Brand3.jpg'),
  require('../../../assets/images/flor_de_lis/Brand4.jpg'),
]

const pageTitle = 'Flor De Lis'

const FlorDeLis = () => (
  <div>
    <Helmet title={pageTitle} />
    <ProjectView name={pageTitle} imageArray={images} />
  </div>
)

export default FlorDeLis
